package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean lastAttackLargeSpell;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.lastAttackLargeSpell = false;
    }

    /**
     * Method adapter from smallSpell to normalAttack.
     * @return String normal attack representation
     */
    @Override
    public String normalAttack() {
        this.lastAttackLargeSpell = false;
        return spellbook.smallSpell();
    }

    /**
     * Method adapter from largeSpell to chargedAttack.
     * @return String normal attack representation
     */
    @Override
    public String chargedAttack() {
        if (!this.lastAttackLargeSpell) {
            this.lastAttackLargeSpell = true;
            return spellbook.largeSpell();
        }
        else {
            return "Can not use largeSpell twice in a row.";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
