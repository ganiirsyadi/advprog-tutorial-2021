package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import java.util.Arrays;

public class CaesarChiperTransformation {

    private int shiftAmount;

    public CaesarChiperTransformation(int shiftAmount){
        this.shiftAmount = shiftAmount;
    }

    public CaesarChiperTransformation(){
        this(5);
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? 1 : -1;
        int n = text.length();
        char[] res = new char[n];
        for (int i = 0; i < res.length; i++) {
            int newIndex;
            if (selector == 1) {
                newIndex = (codex.getIndex(text.charAt(i)) + shiftAmount) % codex.getCharSize();
            } else {
                newIndex = (codex.getIndex(text.charAt(i)) - shiftAmount);
                newIndex = newIndex < 0 ? newIndex + codex.getCharSize() : newIndex;
            }
            res[i] = codex.getChar(newIndex);
        }
        return new Spell(new String(res), codex);
    }
}
