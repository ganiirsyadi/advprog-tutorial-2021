package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class SeawardPride implements Weapon {


    private String holderName;

    public SeawardPride(String holderName) {
        this.holderName = holderName;
    }

    /**
     * Method to execute Normal Attack.
     * @return String normal attack representation
     */
    @Override
    public String normalAttack() {
        return "Normal attack from seaward with the help of Magikarp";
    }

    /**
     * Method to execute Charged Attack.
     * @return String charged attack representation
     */
    @Override
    public String chargedAttack() {
        return "Charged attack from seaward with the help of Gyarados";
    }

    @Override
    public String getName() {
        return "Seaward Pride";
    }

    @Override
    public String getHolderName() { return holderName; }
}
