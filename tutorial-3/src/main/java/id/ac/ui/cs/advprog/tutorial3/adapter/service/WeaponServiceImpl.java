package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    /**
     * Method to findAllWeapons.
     * @return List of all weapons.
     */
    @Override
    public List<Weapon> findAll() {

        for (Spellbook spellbook: spellbookRepository.findAll()) {
            if (weaponRepository.findByAlias(spellbook.getName()) == null) {
                weaponRepository.save(new SpellbookAdapter(spellbook));
            }
        }

        for (Bow bow: bowRepository.findAll()) {
            if (weaponRepository.findByAlias(bow.getName()) == null) {
                weaponRepository.save(new BowAdapter(bow));
            }
        }

        return weaponRepository.findAll();
    }

    /**
     * Method to execute attack with Weapon.
     * @param weaponName weapon name
     * @param attackType attack type, the option is 0 for normal attack
     *                   and 1 for charged attack
     */
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weaponFound = null;
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        Bow bow = bowRepository.findByAlias(weaponName);
        Spellbook spellbook = spellbookRepository.findByAlias(weaponName);
        if (weapon != null) {
            weaponFound = weapon;
        } else if (bow != null) {
            weaponFound = new BowAdapter(bow);
        } else if (spellbook != null) {
            weaponFound = new SpellbookAdapter(spellbook);
        }
        if (weaponFound != null) {
            weaponRepository.save(weaponFound);
            logRepository.addLog(attackType == 0 ? weaponFound.normalAttack() : weaponFound.chargedAttack());
        }
    }

    /**
     * Method to get all attack logs.
     * @return List of all attack logs
     */
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
