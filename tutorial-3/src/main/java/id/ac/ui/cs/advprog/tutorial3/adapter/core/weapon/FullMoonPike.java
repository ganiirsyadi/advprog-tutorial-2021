package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class FullMoonPike implements Weapon {

    private String holderName;

    public FullMoonPike(String holderName) {
        this.holderName = holderName;
    }

    /**
     * Method to execute Normal Attack.
     * @return String normal attack representation
     */
    @Override
    public String normalAttack() {
        return "Normal attack from Full Moon Pike, the moon is not yet completely full";
    }

    /**
     * Method to execute Charged Attack.
     * @return String charged attack representation
     */
    @Override
    public String chargedAttack() {
        return "Charged attack from Full Moon Pike, the moon is now completely Full";
    }

    @Override
    public String getName() {
        return "Full Moon Pike";
    }

    @Override
    public String getHolderName() { return holderName; }
}
