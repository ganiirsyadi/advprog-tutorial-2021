package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarChiperTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;

public class Translator {
    AbyssalTransformation abyssalTransformation;
    CaesarChiperTransformation caesarChiperTransformation;
    CelestialTransformation celestialTransformation;

    public Translator() {
        this.abyssalTransformation = new AbyssalTransformation();
        this.caesarChiperTransformation = new CaesarChiperTransformation();
        this.celestialTransformation = new CelestialTransformation();
    }

    public String encode(String text) {
        Spell spell = new Spell(text, AlphaCodex.getInstance());
        spell = caesarChiperTransformation.encode(spell);
        spell = celestialTransformation.encode(spell);
        spell = abyssalTransformation.encode(spell);
        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());
        return spell.getText();
    }

    public String decode(String code) {
        Spell spell = new Spell(code, RunicCodex.getInstance());
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());
        spell = abyssalTransformation.decode(spell);
        spell = celestialTransformation.decode(spell);
        spell = caesarChiperTransformation.decode(spell);
        return spell.getText();
    }
}
