package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class FesteringGreed implements Weapon {

    private String holderName;

    public FesteringGreed(String holderName) {
        this.holderName = holderName;
    }

    /**
     * Method to execute Normal Attack.
     * @return String normal attack representation
     */
    @Override
    public String normalAttack() {
        return "Normal attack from Festering Greed, piyu piyu";
    }

    @Override
    public String chargedAttack() {
        return "Charged attack from Festering Greed, POWW POWWW";
    }

    @Override
    public String getName() {
        return "Festering Greed";
    }

    @Override
    public String getHolderName() { return holderName; }
}
