package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class StaffOfHoumo implements Weapon {

    private String holderName;

    public StaffOfHoumo(String holderName) {
        this.holderName = holderName;
    }

    /**
     * Method to execute Normal Attack.
     * @return String normal attack representation
     */
    @Override
    public String normalAttack() {
        return "Normal attack from staff of Houmo, hoom hoom";
    }

    /**
     * Method to execute Charged Attack.
     * @return String charged attack representation
     */
    @Override
    public String chargedAttack() {
        return "Charged attack from staff of houmo, HOOOMMMM";
    }

    @Override
    public String getName() {
        return "Staff of Houmo";
    }

    @Override
    public String getHolderName() { return holderName; }
}
