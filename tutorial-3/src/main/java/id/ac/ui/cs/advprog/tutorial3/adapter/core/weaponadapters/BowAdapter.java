package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean aimShotMode;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.aimShotMode = false;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(this.aimShotMode);
    }

    @Override
    public String chargedAttack() {
        this.aimShotMode = !this.aimShotMode;
        String newMode = this.aimShotMode ? "Aim Shot Mode" : "Spontan Mode";
        return "Change mode to " + newMode;
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}
