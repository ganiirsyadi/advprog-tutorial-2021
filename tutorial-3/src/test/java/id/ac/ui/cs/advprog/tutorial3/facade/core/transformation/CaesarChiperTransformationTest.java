package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CaesarChiperTransformationTest {
    private Class<?> caesarChiperClass;

    @BeforeEach
    public void setup() throws Exception {
        caesarChiperClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarChiperTransformation");
    }

    @Test
    public void testCaesarChiperHasEncodeMethod() throws Exception {
        Method translate = caesarChiperClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCaesarChiperEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "XfknwfEfsiENE1jsyEytEfEgqfhpxrnymEytEktwljEtzwEx1twi";

        Spell result = new CaesarChiperTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarChiperEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "UchktcBcpfBKBygpvBvqBcBdncemuokvjBvqBhqtigBqwtBuyqtf";

        Spell result = new CaesarChiperTransformation(2).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarChiperHasDecodeMethod() throws Exception {
        Method translate = caesarChiperClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCaesarChiperDecodesCorrectly() throws Exception {
        String text = "XfknwfEfsiENE1jsyEytEfEgqfhpxrnymEytEktwljEtzwEx1twi";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new CaesarChiperTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarChiperDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "UchktcBcpfBKBygpvBvqBcBdncemuokvjBvqBhqtigBqwtBuyqtf";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new CaesarChiperTransformation(2).decode(spell);
        assertEquals(expected, result.getText());
    }
}
