package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TranslatorTest {

    private Class<?> translatorClass;

    @BeforeEach
    public void setup() throws Exception {
        translatorClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Translator");
    }

    @Test
    public void testTranslatorHasEncodeMethod() throws Exception {
        Method translate = translatorClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTranslatorHasDecodeMethod() throws Exception {
        Method translate = translatorClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTranslatorEncodesCorrectly() throws Exception {
        String text = "Magikarp lebih OP daripada Gyrados";
        String expected = "_aA;|pcF^DmF*wAFsH_uCnysZDH[ddveo&";
        String result = new Translator().encode(text);
        assertEquals(expected, result);
    }

    @Test
    public void testTranslatorDecodesCorrectly() throws Exception {
        String text = "_aA;|pcF^DmF*wAFsH_uCnysZDH[ddveo&";
        String expected = "Magikarp lebih OP daripada Gyrados";
        String result = new Translator().decode(text);
        assertEquals(expected, result);
    }
}
