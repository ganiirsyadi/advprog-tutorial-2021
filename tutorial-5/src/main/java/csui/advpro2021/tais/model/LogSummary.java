package csui.advpro2021.tais.model;

import lombok.*;

import java.text.DateFormatSymbols;

@Data
public class LogSummary {
    private String month;
    private double jamKerja;
    private long pembayaran;

    public LogSummary(int month, double jamKerja) {
        this.month = new DateFormatSymbols().getMonths()[month];
        this.jamKerja = jamKerja;
        this.pembayaran = (long) jamKerja * 350;
    }
}
