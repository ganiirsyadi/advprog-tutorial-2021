package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", updatable = false, nullable = false)
    private int idLog;

    @Column(name = "start_time")
    @JsonFormat(pattern="dd-MM-yyyy HH:mm")
    private LocalDateTime start;

    @Column(name = "end_time")
    @JsonFormat(pattern="dd-MM-yyyy HH:mm")
    private LocalDateTime end;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "npm")
    @JsonIgnoreProperties({"nama","email","ipk","noTelp","mataKuliah"})
    private Mahasiswa mahasiswa;

    public long getDurasiMenit() {
        return Duration.between(start, end).toMinutes();
    }

    @JsonCreator
    public Log(
            @JsonProperty("start") String start,
            @JsonProperty("end") String end,
            @JsonProperty("description") String description
    ) {
        this.start = LocalDateTime.parse(start, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
        this.end = LocalDateTime.parse(end, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
        this.description = description;
    }
}
