package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {
    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private MataKuliahService mataKuliahService;

    @Override
    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa) {
        mahasiswa.setLogs(new ArrayList<>());
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public Iterable<Mahasiswa> getListMahasiswa() {
        return mahasiswaRepository.findAll();
    }

    @Override
    public Mahasiswa getMahasiswaByNPM(String npm) {
        return mahasiswaRepository.findByNpm(npm);
    }

    @Override
    public Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa) {
        mahasiswa.setNpm(npm);
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public void deleteMahasiswaByNPM(String npm) {
        mahasiswaRepository.deleteById(npm);
    }

    @Override
    public Mahasiswa registerToMatkul(String npm, String kodeMatkul) {
        Mahasiswa mahasiswa = this.getMahasiswaByNPM(npm);
        MataKuliah mataKuliah = mataKuliahService.getMataKuliah(kodeMatkul);

        mahasiswa.setMataKuliah(mataKuliah);
        mataKuliah.getAsistenDosen().add(mahasiswa);

        mahasiswaRepository.save(mahasiswa);
        mataKuliahRepository.save(mataKuliah);
        return mahasiswa;
    }
}
