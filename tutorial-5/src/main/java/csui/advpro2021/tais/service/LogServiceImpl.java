package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public Log createLog(Log log, String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);

        log.setMahasiswa(mahasiswa);
        mahasiswa.getLogs().add(log);

        mahasiswaRepository.save(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Log getLogById(int id) {
        return logRepository.findById(id);
    }

    @Override
    public Log updateLog(int id, Log log) {
        Log logFound = logRepository.findById(id);
        logFound.setStart(log.getStart());
        logFound.setEnd(log.getEnd());
        logFound.setDescription(log.getDescription());
        logRepository.save(logFound);
        return logFound;
    }

    @Override
    public void deleteLog(int id) {
        logRepository.deleteById(id);
    }



    @Override
    public Iterable<LogSummary> getLogSummaryByNpm(String npm) {
        List<LogSummary> logSummaries = new ArrayList<LogSummary>();

        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        List<Log> logs = logRepository.findByMahasiswa(mahasiswa);

        double[] jamKerjaBulanan = new double[12];

        for (Log log:logs) {
            int month = log.getStart().getMonthValue() - 1;
            jamKerjaBulanan[month] += log.getDurasiMenit();
        }

        for (int i = 0; i < 12; i++) {
            logSummaries.add(new LogSummary(i, jamKerjaBulanan[i] / 60));
        }

        return logSummaries;
    }
}
