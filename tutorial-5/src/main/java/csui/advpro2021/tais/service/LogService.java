package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;

public interface LogService {
    Log createLog(Log log, String npm);

    Log getLogById(int id);

    Log updateLog(int id, Log log);

    void deleteLog(int id);

    Iterable<LogSummary> getLogSummaryByNpm(String npm);
}
