package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/log")
public class LogController {

    @Autowired
    private LogService logService;

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Log> getLogById(@PathVariable(value= "id") int id) {
        return ResponseEntity.ok(logService.getLogById(id));
    }

    @GetMapping(path = "/mahasiswa/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<LogSummary>> getLogSummaryByNpm(@PathVariable(value="npm") String npm) {
        return ResponseEntity.ok(logService.getLogSummaryByNpm(npm));
    }

    @PostMapping(path = "/create/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Log> createLog(@PathVariable(value="npm") String npm, @RequestBody Log log) {
        return ResponseEntity.ok(logService.createLog(log, npm));
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Log> updateLog(@PathVariable(value="id") int id, @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(id, log));
    }

    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<String> deleteLog(@PathVariable(value="id") int id) {
        logService.deleteLog(id);
        return ResponseEntity.status(HttpStatus.OK).body("Deleted successfully");
    }
}
