package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import csui.advpro2021.tais.service.MataKuliahServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = LogController.class)
class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    private Log log;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa(
                "1906350616",
                "Gani Ilham Irsyadi",
                "ganiilhamirsyadi@gmail.com",
                "3.5",
                "08123456789");
        log = new Log("21-03-2021 09:00", "21-03-2021 10:30", "koreksi TP 1");
        log.setMahasiswa(mahasiswa);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private String mapLogToJson(Log log) throws JsonProcessingException {
        return String.format("{\"start\": \"21-03-2021 09:00\", \"end\": \"21-03-2021 10:30\", \"description\": \"%s\"}", log.getDescription());
    }

    @Test
    public void testControllerCreateLog() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(logService.createLog(any(Log.class), any(String.class))).thenReturn(log);
        mvc.perform(post("/log/create/1906350616")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapLogToJson(log)))
                .andExpect(jsonPath("$.idLog").value("0"));
    }

    @Test
    public void testControllerUpdateLog() throws Exception {
        log.setDescription("New Description");
        when(logService.updateLog(anyInt(),  any(Log.class))).thenReturn(log);
        mvc.perform(put("/log/0").contentType(MediaType.APPLICATION_JSON).content(mapLogToJson(log)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description").value("New Description"));
    }

    @Test
    public void testControllerDeleteLog() throws Exception {
        mvc.perform(delete("/log/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().string("Deleted successfully"));
    }

    @Test
    public void testControllerGetLogById() throws Exception {
        when(logService.getLogById(0)).thenReturn(log);
        mvc.perform(get("/log/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.idLog").value("0"));
    }

    @Test
    public void testControllerGetLogSummaryByNpm() throws Exception {
        LogSummary logSummary = new LogSummary(0, 3.5);
        logSummary.setJamKerja(2);
        String month = logSummary.getMonth();
        Iterable<LogSummary> summaries = Collections.singletonList(logSummary);
        when(logService.getLogSummaryByNpm("1906350616")).thenReturn(summaries);
        mvc.perform(get("/log/mahasiswa/1906350616").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapToJson(summaries)))
                .andExpect(jsonPath("$[0].month").value(month));
    }
}