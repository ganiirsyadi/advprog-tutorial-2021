package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;


import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private MahasiswaServiceImpl mahasiswaService;

    @InjectMocks
    private LogServiceImpl logService;

    private Log log;
    private Log log2;
    private Mahasiswa mahasiswa;
    private LogSummary logSummary;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        log = new Log("21-01-2021 09:00", "21-01-2021 10:30", "koreksi TP 1");
        log.setMahasiswa(mahasiswa);

        log2 = new Log("21-01-2021 09:00", "21-01-2021 10:30", "koreksi TP 2");
        log2.setMahasiswa(mahasiswa);

        logSummary = new LogSummary(
                log.getStart().getMonthValue(),
                log.getDurasiMenit() / 60.0
        );
    }

    @Test
    public void testServiceCreateLog() {
        mahasiswa.setLogs(new ArrayList<>());
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(logService.createLog(log, mahasiswa.getNpm())).thenReturn(log);

        when(mahasiswaRepository.save(any(Mahasiswa.class))).thenReturn(mahasiswa);

        Log result = logService.createLog(log, mahasiswa.getNpm());
        assertEquals(result.getDescription(), log.getDescription());
    }

    @Test
    public void testServiceGetLogById() {
        when(logRepository.findById(0)).thenReturn(log);
        Log result = logService.getLogById(0);
        assertEquals(result.getDescription(), log.getDescription());
    }

    @Test
    public void testServiceUpdateLog() {
        when(logRepository.findById(0)).thenReturn(log);
        Log result = logService.updateLog(0, log2);
        assertEquals(result.getDescription(), log2.getDescription());
    }

    @Test
    public void testServiceDeleteLog() {
        mahasiswa.setLogs(new ArrayList<>());
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(logService.createLog(log, mahasiswa.getNpm())).thenReturn(log);

        when(mahasiswaRepository.save(any(Mahasiswa.class))).thenReturn(mahasiswa);

        logService.createLog(log, mahasiswa.getNpm());

        logService.deleteLog(0);

        assertNull(logService.getLogById(0));
    }

    @Test
    public void testServiceGetLogSummaryByNpm() {
        List<Log> logs = new ArrayList<>();
        logs.add(log);
        when(logRepository.findByMahasiswa(mahasiswa)).thenReturn(logs);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        Iterable<LogSummary> logSummaries = logService.getLogSummaryByNpm(mahasiswa.getNpm());
        assertEquals(logSummaries.iterator().next().getPembayaran(), logSummary.getPembayaran());
    }
}
