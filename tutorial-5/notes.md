## Notes

Setelah membaca percakapan pada soal, 
saya mencoba untuk menggambarkan `Database Diagram` berdasarkan percakapan
tersebut.

Gambar Database Diagram:

![DBDiagram](./image/dbdiagram.png)

Selain itu, user juga menginginkan adanya endpoint khusus untuk
mendapatkan **rangkuman log setiap bulannya**.

Berikut adalah API Documentation setelah mengimplement semua requirements tersebut,
[API Documentation](https://documenter.getpostman.com/view/11246885/TzCQbmrG)
 