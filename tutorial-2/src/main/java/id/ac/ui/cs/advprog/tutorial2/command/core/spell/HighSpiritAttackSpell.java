package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritAttackSpell extends HighSpiritSpell {

    /**
     * Constructor.
     * @param highSpirit spirit.
     */
    public HighSpiritAttackSpell(HighSpirit highSpirit) {
        super(highSpirit);
    }

    /**
     * Calling attackStance() method from High Spirit.
     */
    @Override
    public void cast() {
        spirit.attackStance();
    }

    /**
     * Generate string representation.
     * @return String the name of the spell.
     */
    @Override
    public String spellName() {
        return spirit.getRace() + ":Attack";
    }
}
