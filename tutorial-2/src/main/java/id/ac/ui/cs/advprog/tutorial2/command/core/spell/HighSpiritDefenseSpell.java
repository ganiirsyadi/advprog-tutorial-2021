package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritDefenseSpell extends HighSpiritSpell {

    /**
     * Constructor.
     * @param highSpirit spirit.
     */
    public HighSpiritDefenseSpell(HighSpirit highSpirit) {
        super(highSpirit);
    }

    /**
     * Calling defenseStance() method from High Spirit.
     */
    @Override
    public void cast() {
        spirit.defenseStance();
    }

    /**
     * Generate string representation.
     * @return String the name of the spell.
     */
    @Override
    public String spellName() {
        return spirit.getRace() + ":Defense";
    }
}
