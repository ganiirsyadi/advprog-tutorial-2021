package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // Initiate ArrayList that contains the spells
    ArrayList<Spell> spellList;

    /**
     * Constructor.
     * @param spellList ArrayList containing spells.
     */
    public ChainSpell(ArrayList<Spell> spellList) {
        this.spellList = spellList;
    }

    /**
     * Cast each of the spell in spellList.
     */
    @Override
    public void cast() {
        for (Spell spell : spellList) {
            spell.cast();
        }
    }

    /**
     * Undo each spell in spellList backward.
     */
    @Override
    public void undo() {
        for(int i = spellList.size() - 1; i >= 0; i--) {
            spellList.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
