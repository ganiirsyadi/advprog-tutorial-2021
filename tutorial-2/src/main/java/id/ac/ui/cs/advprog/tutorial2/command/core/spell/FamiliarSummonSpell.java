package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public class FamiliarSummonSpell extends FamiliarSpell {

    /**
     * Constructor.
     * @param familiar spirit.
     */
    public FamiliarSummonSpell(Familiar familiar) {
        super(familiar);
    }

    /**
     * Calling summon function from familiar spirit.
     */
    @Override
    public void cast() {
        familiar.summon();
    }

    /**
     * Generate string representation.
     * @return String the name of the spell.
     */
    @Override
    public String spellName() {
        return familiar.getRace() + ":Summon";
    }
}
