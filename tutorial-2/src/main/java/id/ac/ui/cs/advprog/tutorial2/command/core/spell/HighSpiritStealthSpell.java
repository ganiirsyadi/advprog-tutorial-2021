package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritStealthSpell extends HighSpiritSpell {

    /**
     * Constructor.
     * @param highSpirit spirit.
     */
    public HighSpiritStealthSpell(HighSpirit highSpirit) {
        super(highSpirit);
    }

    /**
     * Calling stealthStance() method from High Spirit.
     */
    @Override
    public void cast() {
        spirit.stealthStance();
    }

    /**
     * Generate string representation.
     * @return String the name of the spell.
     */
    @Override
    public String spellName() {
        return spirit.getRace() + ":Stealth";
    }
}
