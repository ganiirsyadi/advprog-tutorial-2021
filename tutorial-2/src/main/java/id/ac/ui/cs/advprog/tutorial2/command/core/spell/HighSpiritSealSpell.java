package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritSealSpell extends HighSpiritSpell {

    /**
     * Constructor.
     * @param highSpirit spirit.
     */
    public HighSpiritSealSpell(HighSpirit highSpirit) {
        super(highSpirit);
    }

    /**
     * Calling seal() method from High Spirit.
     */
    @Override
    public void cast() {
        spirit.seal();
    }


    /**
     * Generate string representation.
     * @return String the name of the spell.
     */
    @Override
    public String spellName() {
        return spirit.getRace() + ":Seal";
    }
}
