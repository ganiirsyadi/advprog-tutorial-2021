package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public class FamiliarSealSpell extends FamiliarSpell {

    /**
     * Constructor.
     * @param familiar spirit.
     */
    public FamiliarSealSpell(Familiar familiar) {
        super(familiar);
    }

    /**
     * Calling seal function from familiar spirit.
     */
    @Override
    public void cast() {
        familiar.seal();
    }

    /**
     * Generate string representation.
     * @return String the name of the spell.
     */
    @Override
    public String spellName() {
        return familiar.getRace() + ":Seal";
    }
}
