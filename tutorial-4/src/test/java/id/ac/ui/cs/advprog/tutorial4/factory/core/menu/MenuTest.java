package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.InuzumaRamenFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MenuTest {
    private Class<?> menuClass;
    private Menu menu;

    @BeforeEach
    public void setUp() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
        menu = new Menu("RestaurantMenu", new InuzumaRamenFactory());
    }

    @Test
    public void testMenuIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(menuClass.getModifiers()));
    }

    @Test
    public void testMenuGetNameReturnCorrectly() throws Exception {
        String name = menu.getName();
        assertEquals(name, "RestaurantMenu");
    }

    @Test
    public void testMenuGetNoodleReturnCorrectly() throws Exception {
        Noodle noodle = menu.getNoodle();
        assertNotNull(noodle);
    }

    @Test
    public void testMenuGetMeatReturnCorrectly() throws Exception {
        Meat meat = menu.getMeat();
        assertNotNull(meat);
    }

    @Test
    public void testMenuGetToppingReturnCorrectly() throws Exception {
        Topping topping = menu.getTopping();
        assertNotNull(topping);
    }

    @Test
    public void testMenuGetFlavorReturnCorrectly() throws Exception {
        Flavor flavor = menu.getFlavor();
        assertNotNull(flavor);
    }

}
