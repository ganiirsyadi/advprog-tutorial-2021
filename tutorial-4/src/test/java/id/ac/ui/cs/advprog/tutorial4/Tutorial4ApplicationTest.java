package id.ac.ui.cs.advprog.tutorial4;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class Tutorial4ApplicationTest {
    @Test
    void contextLoads() {
    }
    @Test
    void testTutorial4ApplicationCanBeRun() {
        String[] args = new String[]{};
        Tutorial4Application.main(args);
    }
}
