package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class MenuServiceImplTest {
    private MenuServiceImpl menuService;

    @Mock
    MenuRepository menuRepository;


    @BeforeEach
    public void setup() throws Exception {
        menuService = new MenuServiceImpl();
    }

    @Test
    public void testMenuServiceGetMenusReturnCorrectMenuAmount() {
        assertEquals(4, menuService.getMenus().size());
    }

    @Test
    public void testMenuServiceCreateMenuImplemented() {
        menuService.createMenu("New Udon", "Udon");
        menuService.createMenu("New Shirataki", "Shirataki");
        menuService.createMenu("New Ramen", "Ramen");
        menuService.createMenu("New Soba", "Soba");
        assertEquals(8, menuService.getMenus().size());
    }
}
