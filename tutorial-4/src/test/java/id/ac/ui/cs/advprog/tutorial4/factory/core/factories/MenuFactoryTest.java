package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuFactoryTest {
    private Class<?> menuFactoryInterface;

    @BeforeEach
    public void setup() throws Exception {
        menuFactoryInterface = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factories.MenuFactory");
    }

    @Test
    public void testMenuFactoryIsAPublicInterface() {
        int classModifiers = menuFactoryInterface.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testMenuFactoryHasCreateFlavorAbstractMethod() throws Exception {
        Method getDescription = menuFactoryInterface.getDeclaredMethod("createFlavor");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testMenuFactoryHasCreateMeatAbstractMethod() throws Exception {
        Method getDescription = menuFactoryInterface.getDeclaredMethod("createMeat");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testMenuFactoryHasCreateToppingAbstractMethod() throws Exception {
        Method getDescription = menuFactoryInterface.getDeclaredMethod("createTopping");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testMenuFactoryHasCreateNoodleAbstractMethod() throws Exception {
        Method getDescription = menuFactoryInterface.getDeclaredMethod("createNoodle");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }
}
