package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaFactoryTest {
    private Class<?> liyuanSobaClass;
    private LiyuanSobaFactory liyuanSoba;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factories.LiyuanSobaFactory");
        liyuanSoba = new LiyuanSobaFactory();
    }

    @Test
    public void testLiyuanSobaFactoryIsAConcreteClass() {
        assertFalse(Modifier.
                isAbstract(liyuanSobaClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factories.MenuFactory")));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideCreateNoodle() throws Exception {
        Method getDescription = liyuanSobaClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideCreateMeat() throws Exception {
        Method getDescription = liyuanSobaClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideCreateTopping() throws Exception {
        Method getDescription = liyuanSobaClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideCreateFlavor() throws Exception {
        Method getDescription = liyuanSobaClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryCreateNoodleReturnCorrectly() throws Exception {
        Noodle noodle = liyuanSoba.createNoodle();
        String description = noodle.getDescription();
        assertEquals(description, "Adding Liyuan Soba Noodles...");
    }

    @Test
    public void testLiyuanSobaFactoryCreateMeatReturnCorrectly() throws Exception {
        Meat meat = liyuanSoba.createMeat();
        String description = meat.getDescription();
        assertEquals(description, "Adding Maro Beef Meat...");
    }

    @Test
    public void testLiyuanSobaFactoryCreateToppingReturnCorrectly() throws Exception {
        Topping topping = liyuanSoba.createTopping();
        String description = topping.getDescription();
        assertEquals(description, "Adding Shiitake Mushroom Topping...");
    }

    @Test
    public void testLiyuanSobaFactoryCreateFlavorReturnCorrectly() throws Exception {
        Flavor flavor = liyuanSoba.createFlavor();
        String description = flavor.getDescription();
        assertEquals(description, "Adding a dash of Sweet Soy Sauce...");
    }
}
