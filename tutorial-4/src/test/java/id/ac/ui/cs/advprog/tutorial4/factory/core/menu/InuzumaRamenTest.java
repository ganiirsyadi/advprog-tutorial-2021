package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.InuzumaRamenFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;
    private InuzumaRamen inuzumaRamen;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
        inuzumaRamen = new InuzumaRamen("RestaurantInuzumaRamen");
    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(inuzumaRamenClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenGetNameReturnCorrectly() throws Exception {
        String name = inuzumaRamen.getName();
        assertEquals(name, "RestaurantInuzumaRamen");
    }

    @Test
    public void testInuzumaRamenGetNoodleReturnCorrectly() throws Exception {
        Noodle noodle = inuzumaRamen.getNoodle();
        assertNotNull(noodle);
    }

    @Test
    public void testInuzumaRamenGetMeatReturnCorrectly() throws Exception {
        Meat meat = inuzumaRamen.getMeat();
        assertNotNull(meat);
    }

    @Test
    public void testInuzumaRamenGetToppingReturnCorrectly() throws Exception {
        Topping topping = inuzumaRamen.getTopping();
        assertNotNull(topping);
    }

    @Test
    public void testInuzumaRamenGetFlavorReturnCorrectly() throws Exception {
        Flavor flavor = inuzumaRamen.getFlavor();
        assertNotNull(flavor);
    }

}
