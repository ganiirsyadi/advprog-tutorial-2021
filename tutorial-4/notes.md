## Notes
Baik Lazy Instantiation maupun Eager Instantiation 
merupakan cara untuk menginstansiasi sebuah objek 
pada sebuah Singleton Class.

#### Lazy Instantiation
Lazy Instantiation adalah cara menginstansiasi objek hanya sekali 
ketika Class tersebut digunakan. Pada contoh kasus `tutorial-4` hal ini dilakukan
ketika method `getInstance()` pertama kali dipanggil. 
Artinya, apabila method `getInstance()` tidak pernah dipanggil, maka objek
tidak akan diinstansiasi.

Keuntungan:

- Menghemat memori karena instansiasi objek hanya dilakukan ketika dibutuhkan.
- Mengurangi instansiasi objek yang tidak perlu.

Kekurangan:

- Dapat menjadi performance issue ketika menggunakan multithreaded environment
menggunakan JVM karena dapat terjadi pemanggilan `getInstance()` pada waktu yang bersamaan.
- Selalu melakukan pengecekan setiap kali pemanggilan `getInstance()`

#### Eager Instantiation
Eager Instantiation berbeda dengan Lazy Instantiation karena instansiasi object pasti
dilakukan ketika class di-load dengan cara menginisiasi pada static variable. 
JVM menjamin bahwa instance dari class tersebut sudah dibuat sebelum thread manapun
mengakses static variable dari instance.

Keuntungan:

- Aman dari isu pada multithreading karena instansiasi object dilakukan oleh JVM
ketika Class di-load (sebelum ada thread yang mengakses static variable dari instance).
- Pada setiap pemanggilan method `getInstance()` tidak diperlukan lagi pengecekan. 
Instance dapat langsung direturn.

Kekurangan:

- Jika ternyata Singleton tersebut belum tentu digunakan maka instansiasi tetap
dilakukan dan hal tersebut pasti menggunakan resource yang cukup banyak.
- Overhead akan semakin besar apabila penggunaan Singleton juga banyak.

