package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    // Constructor
    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
    }

    // Update Agile Adventurer's quest (only accept Delivery and Escort)
    @Override
    public void update() {
        if (this.guild.getQuestType().equals("D") ||
                this.guild.getQuestType().equals("E")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
