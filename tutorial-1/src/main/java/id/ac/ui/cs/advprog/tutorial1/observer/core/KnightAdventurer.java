package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

    // Constructor
    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
    }

    // Update Agile Adventurer's quest (accept all quests)
    @Override
    public void update() {
        this.getQuests().add(this.guild.getQuest());
    }
}
