package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {

    @Override
    public String defend() {
        return "Defend with Shield, tass tass!";
    }

    @Override
    public String getType() {
        return "Defense Behavior - Shield";
    }
}
