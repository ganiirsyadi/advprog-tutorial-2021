package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {

    @Override
    public String attack() {
        return "Attack with Gun, dor dor!";
    }

    @Override
    public String getType() {
        return "Attack Behavior - Gun";
    }
}
