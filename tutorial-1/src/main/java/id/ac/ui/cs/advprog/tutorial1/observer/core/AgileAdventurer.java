package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    // Constructor
    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
    }

    // Update Agile Adventurer's quest (only accept Delivery and Rumble)
    @Override
    public void update() {
        if (this.guild.getQuestType().equals("D") ||
                this.guild.getQuestType().equals("R")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
