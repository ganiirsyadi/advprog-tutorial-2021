package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {

    @Override
    public String attack() {
        return "Attack with Magic, clingg clingg!";
    }

    @Override
    public String getType() {
        return "Attack Behavior - Magic";
    }
}
