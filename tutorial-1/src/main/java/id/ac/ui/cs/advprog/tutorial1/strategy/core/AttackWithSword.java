package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {

    @Override
    public String attack() {
        return "Attack with Sword, swoosh swoosh!";
    }

    @Override
    public String getType() {
        return "Attack Behavior - Sword";
    }
}
