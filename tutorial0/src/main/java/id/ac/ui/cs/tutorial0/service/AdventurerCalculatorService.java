package id.ac.ui.cs.tutorial0.service;

public interface AdventurerCalculatorService {
    /**
     * Count power based on birthYear
     * @param birthYear the birth year
     * @return int power as a result
     */
    public int countPowerPotensialFromBirthYear(int birthYear);

    /**
     * Categorize power based on its value
     * @param birthYear the birth year
     * @return string power with its class category
     */
    public String powerClassifier(int birthYear);
}
